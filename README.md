# 适配版本一览表

| chainmaker-go | chainmaker-explorer-backend | 
|:--------------|:----------------------------| 
| v1.0.0        | v1.1.1                      | 
| v1.1.0        | v1.1.1                      | 
| v1.1.1        | v1.1.1                      | 
| v1.2.0        | v1.1.1                      | 
| v1.2.3        | v1.1.1                      | 
| v1.2.4        | v1.1.1                      | 
| v1.2.5        | v1.1.1                      | 
| v2.0.0        | v2.0.0                      | 
| v2.0+         | v2.1.0                      | 
| v2.0+         | v2.2.0                      |
| v2.0+         | v2.3.0                      |
| v2.0+         | v2.3.1                      |

测试接口
> curl localhost:9997/chainmaker?cmb=Decimal
